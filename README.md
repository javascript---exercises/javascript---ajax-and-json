# JavaScript - AJAX and JSON

This project contains 2 exercises made with JavaScript, AJAX and JSON

Exercise 1

 - Idea of this exercise was to make web application for selling houses. Techniques used to make this was HTML, CSS, JavaScript, AJAX and JSON. There are 5 houses and all of their data is in JSON file. Application uses sripts to print JSON data to the page. You can find the working application from this url https://student.labranet.jamk.fi/~K9452/ttms0500-syksy2019-harjoitukset/Harjoitukset4/Teht%c3%a4v%c3%a4%201.html
 
 Excercise 2
 
  - Idea of this exercise was to make web application for printing names from file called ajax-suggest.php. Application searches firstnames from the file and prints them. Techniques used in here are HTML, CSS, JavaScript and AJAX. You can find the working application from this url https://student.labranet.jamk.fi/~K9452/ttms0500-syksy2019-harjoitukset/Harjoitukset4/Teht%c3%a4v%c3%a4%202.html